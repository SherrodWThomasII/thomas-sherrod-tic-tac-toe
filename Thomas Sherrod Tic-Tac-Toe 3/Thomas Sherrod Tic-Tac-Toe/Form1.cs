﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Thomas_Sherrod_Tic_Tac_Toe
{
    public partial class Form1 : Form
    {
        bool turns = true; // tells if its X's turn or O's turn
        

        public Form1()
        {
            InitializeComponent();
        }

        private void clickXO(object sender, EventArgs e)
        {
            Button btn = (Button)sender; // Casting the objects for the buttons
            if (turns)       
                btn.Text = "X"; // When the turns start, it will begin with X        
            else   
                btn.Text = "O"; // When the next turn starts, it will be O
            turns = !turns;
            btn.Enabled = false;
            
        }
    }
}
