﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Thomas_Sherrod_Tic_Tac_Toe
{
    public partial class Form1 : Form
    {
        bool turns = true; // tells if its X's turn or O's turn
        
        public Form1()
        {
            InitializeComponent();          
        }

        private void clickXO(object sender, EventArgs e)
        {
            Button btn = (Button)sender; // Casting the objects for the buttons           
            
            btn.Enabled = false; // Disables button after it is pressed.

            if (btnX != null)
            {// Shows the tic-tac-toe board and hides the options
                btnA1.Show();
                btnA2.Show();
                btnA3.Show();
                btnB1.Show();
                btnB2.Show();
                btnB3.Show();
                btnC1.Show();
                btnC2.Show();
                btnC3.Show();
                btnX.Hide();
                btnO.Hide();
                if (turns)
                    btn.Text = "O";
                else
                    btn.Text = "X";
                turns = !turns; // Makes sure that each turn is different
            }
            if (btnO != null)
            {// Shows the tic-tac-toe board and hides the options
                btnA1.Show();
                btnA2.Show();
                btnA3.Show();
                btnB1.Show();
                btnB2.Show();
                btnB3.Show();
                btnC1.Show();
                btnC2.Show();
                btnC3.Show();
                btnX.Hide();
                btnO.Hide();
                if (turns)
                    btn.Text = "O";
                else
                    btn.Text = "X";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Hides the buttons, until the user chooses X or O.
            btnA1.Hide();
            btnA2.Hide();
            btnA3.Hide();
            btnB1.Hide();
            btnB2.Hide();
            btnB3.Hide();
            btnC1.Hide();
            btnC2.Hide();
            btnC3.Hide();
        }
    }
}
